import asyncio


async def find_divisibles(inrange, div_by):
    print("finding nums in range {} divisible by {}".format(inrange, div_by))
    located = []
    for i in range(inrange):
        if i % div_by == 0:
            located.append(i)

        if i % 100000 == 0:
            await asyncio.sleep(0.0001)
    print("Done w/ nums in range {} divisible by {}".format(inrange, div_by))
    return located


async def main():
    print("Run devs1")
    divs1 = loop.create_task(find_divisibles(508000, 34113))
    print("Run devs2")
    divs2 = loop.create_task(find_divisibles(100052, 3210))
    print("Run devs3")
    divs3 = loop.create_task(find_divisibles(500, 3))
    print("All devs function already run")
    await asyncio.wait([divs1, divs2, divs3])


if __name__ == '__main__':
    # Issue with python 3.11
    # https://stackoverflow.com/questions/73361664/asyncio-get-event-loop-deprecationwarning-there-is-no-current-event-loop
    #loop = asyncio.get_event_loop()
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(main())
    loop.close()
    print("Finish")