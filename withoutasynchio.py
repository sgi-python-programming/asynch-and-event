def find_divisibles(inrange, div_by):
    print("finding nums in range {} divisible by {}".format(inrange, div_by))
    located = []
    for i in range(inrange):
        if i % div_by == 0:
            located.append(i)
    print("Done w/ nums in range {} divisible by {}".format(inrange, div_by))
    return located


def main():
    print("Run devs1")
    divs1 = find_divisibles(508000, 34113)
    print("Run devs2")
    divs2 = find_divisibles(100052, 3210)
    print("Run devs3")
    divs3 = find_divisibles(500, 3)
    print("All devs function already run")


if __name__ == '__main__':
    main()
    print("Finish")